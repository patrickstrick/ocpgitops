# OCPGitOps

## To Do

1. Add GitLab OAuth
2. ~~Add Sealed Secrets~~

## Notes
1. Must give ArgoCD SA cluster-admin in order for it to fully manage cluster resources. 
OpenShift GitOps does not provide that role binding.
